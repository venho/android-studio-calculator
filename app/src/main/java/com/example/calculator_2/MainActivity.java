package com.example.calculator_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void add(View v)
    {
        EditText value1 = (EditText) findViewById(R.id.value1);
        EditText value2 = (EditText) findViewById(R.id.value2);
        EditText result = (EditText) findViewById(R.id.result);

        int v1 = Integer.parseInt(value1.getText().toString());
        int v2 = Integer.parseInt(value2.getText().toString());

        int r = v1 + v2;

        result.setText(r + "");
    }

    public void min(View v)
    {
        EditText value1 = (EditText) findViewById(R.id.value1);
        EditText value2 = (EditText) findViewById(R.id.value2);
        EditText result = (EditText) findViewById(R.id.result);

        int v1 = Integer.parseInt(value1.getText().toString());
        int v2 = Integer.parseInt(value2.getText().toString());

        int r = v1 - v2;

        result.setText(r + "");
    }

    public void mul(View v)
    {
        EditText value1 = (EditText) findViewById(R.id.value1);
        EditText value2 = (EditText) findViewById(R.id.value2);
        EditText result = (EditText) findViewById(R.id.result);

        int v1 = Integer.parseInt(value1.getText().toString());
        int v2 = Integer.parseInt(value2.getText().toString());

        int r = v1 * v2;

        result.setText(r + "");
    }

    public void div(View v)
    {
        EditText value1 = (EditText) findViewById(R.id.value1);
        EditText value2 = (EditText) findViewById(R.id.value2);
        EditText result = (EditText) findViewById(R.id.result);

        int v1 = Integer.parseInt(value1.getText().toString());
        int v2 = Integer.parseInt(value2.getText().toString());

        float r = v1 / (float) v2;

        result.setText(r + "");
    }
}